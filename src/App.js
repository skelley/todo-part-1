import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList";

class App extends Component {
  state = {
    todos: todosList,
    input: ""
  };

  //functions dealing with state have to be here since state is defined on the App
  //we want to pass this delete function down as a component to todolist
  handleChange = event => {
    this.setState({ input: event.target.value });
  };
  handleSubmit = event => {
    if(event.keyCode === 13) {
      if(event.target.value !== ''){
      console.log("test")
      let tempState = this.state
      tempState.todos.push({
        userId: 1,
        id: Math.ceil(Math.random() * 100000),
        title: tempState.input,
        completed: false
      });
      this.setState({ todos: tempState.todos, input: ""})
      event.target.value = ""
    }
    }
  };


  handleToggle = (todo, event) => {
    console.log(todo);
    // const todoCompleted = todo.completed
    todo.completed = !todo.completed;
    let copyofState = this.state;
    this.setState({
      copyofState
    });
  };
  handleDelete = todoIdToDelete => event => {
    //immutablity
    //copy the existing state todos
    //modify the existing state
    //setState  - overwrite the original array with modified copy
    const newTodos = this.state.todos.slice();
    //todoIdToDelete
    //newTodos.findIndex
    const todoIndex = newTodos.findIndex(todo => todo.id === todoIdToDelete);
    newTodos.splice(todoIndex, 1);
    this.setState({ todos: newTodos });
  };
  handleCompleted = event => {
   
    let tempState = this.state
    let tempTodos = tempState.todos.filter(todo => todo.completed === false)
    this.setState({todos: tempTodos})
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onChange={this.handleChange}
            onKeyDown={this.handleSubmit}
          />
        </header>

        <TodoList
          todos={this.state.todos}
          handleDelete={this.handleDelete}
          handleToggle={this.handleToggle}
        />

        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.handleCompleted} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

export default App;
